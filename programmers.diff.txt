arduinoasispleo.name=Arduino as ISP (Leonardo)
arduinoasispleo.communication=serial
arduinoasispleo.protocol=arduino
arduinoasispleo.speed=19200
arduinoasispleo.program.protocol=arduino
arduinoasispleo.program.speed=19200
arduinoasispleo.program.tool=avrdude
arduinoasispleo.program.extra_params=-F -P{serial.port} -b{program.speed}